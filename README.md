# Django Book Store
## 前置作業
- 下載 Git
https://git-scm.com/downloads
- 下載 VSCode
https://code.visualstudio.com/
- 安裝 pipenv：`pip install pipenv`

## 下載專案
- cd 到指定位址 clone 專案
```
git clone https://github.com/kitty88825/study-django-book-store.git
```
- 安裝專案所需的套件( Pipfile 內所有套件)
```
pipenv install
```
- 跑虛擬環境
```
pipenv shell
```
## 修改專案內容
- 使用 VSCode 開啟專案
```
code .
```
- 在 **core 資料夾**下方新增一個 `.env` 檔案
![](https://i.imgur.com/S8AOOhk.png)

- 修改 **core/.env** 檔案的內容
```
SECRET_KEY=xxx
DEBUG=on
ALLOWED_HOSTS=127.0.0.1,localhost
DATABASE_URL=sqlite:///db.sqlite3
```
> 可使用[密鑰生成器](https://miniwebtool.com/zh-tw/django-secret-key-generator/)

- 長出資料
```
python manage.py migrate
```

- 將專案跑起來
```
python manage.py runserver
```

